const reading = { customer: "ivan", quantity: 10, month: 5, year: 2017 };

// Client 1
const aReading = acquireReading();
const baseCharge = aReading.baseCharge;

// Client 2
const rawReading = acquireReading();
const aReading = enrichReading(rawReading);
const taxableCharge = result.taxableCharge;

// Client 3
const rawReading = acquireReading();
const aReading = enrichReading(rawReading);
const basicChargeAmount = aReading.baseCharge;

function calculateBaseCharge(aReading) {
  return baseRate(aReading.month, aReading.year) * aReading.quantity;
}

function enrichReading(original) {
  const result = _.cloneDeep(original);
  result.baseCharge = calculateBaseCharge(result);
  result.taxableCharge = Math.max(0, result.baseCharge - taxThreshold(result.year));
  return result;
}

// Test
it('check reading unchanged', function () {
  const baseReading = { customer: "ivan", quantity: 15, month: 5, year: 2017 };
  const oracle = _.cloneDeep(baseReading);
  enrichReading(baseReading);
  assert.deepEqual(baseReading, oracle);
});