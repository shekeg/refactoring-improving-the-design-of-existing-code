// main.js
import { getDefaultOwner, setDefaultOwner } from './defaultOwner';

spaceship.owner = getDefaultOwner();
setDefaultOwner({firstName: "Rebecca", lastName: "Parsons"});

// defaultOwner.js
let defaultOwner = { firstName: "Martin", lastName: "Fowler" };
export function getDefaultOwner() {return Object.assign({}, defaultOwnerData);}
export function setDefaultOwner(arg) { defaultOwner = arg; }