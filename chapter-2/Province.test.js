const assert = require('assert');
const chai = require('chai');
const expect = chai.expect;

const Province = require('./Province');
const sampleProvinceData = require('./sample-province-data');

describe('province', function () {
  it('shortfall', function () {
    const asia = new Province(sampleProvinceData());
    assert.equal(asia.shortfall, 5);
  });

  it('change production', function () {
    const asia = new Province(sampleProvinceData());
    asia.producers[0].production = 20;
    expect(asia.shortfall).equal(-6);
    expect(asia.profit).equal(292);
  });
});